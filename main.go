// Example main file for a native dapp, replace with application code
package main

import (
	//	"context"

	//	"gitlab.com/go-truffle/enhanced-perigord/migration"

	_ "gitlab.com/go-truffle/perigord-contract-example/migrations"
)

func main() {
	// Run our migrations
	//migration.RunMigrations(context.Background())
}
